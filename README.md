s un desarrollo primitivo de una shell TCP alternativa basada en conexiones mediante sockets con la cual se puede controlar una maquina kuego de ser explotada.
  Objetivos:
    Fácil interactividad
    Indetectable por Antivirus y Firewalls
    Ejecutar comando de forma remota.
