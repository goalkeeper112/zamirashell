var net = require('net'),
	prompt = require('prompt');
	prompt.start();

var cliente	=	prompt.get(['host', 'puerto'], function(err, result){
					if(err){
						throw err;
					}

					var	host = result.host,
						port = result.puerto,
						socket = net.createConnection(port, host);

					console.log("Se establecio conexión en: " + host);
					
					socket.on('error', function(err){
						if(err){
							throw err;
						}
					});

					socket.on('data', function(data){
						prompt.get(['comando'], function(err, result){
							if(err){
								throw err;
							}
							socket.write(result.comando);
							
						});
					});

					socket.on('end', function(){
						console.log('Se a Desconectado del servidor');
					});
				});

cliente.exports = cliente;