var	sys  = require('sys'),
	exec = require('child_process').exec,
	prompt = require('prompt'),
	fs   = require('fs'),
	log,
	net  = require('net');

	function puts(error, stdout, stderr) { 
		sys.puts(stdout); 
	}

var serverTCP = net.createServer(function(socket){

					host = socket.remoteAddress;

					socket.write("Bienvenido al server tcp " + host + "\n");
					
					socket.write("Digite su comando: ");
					
					socket.pipe(socket);

					socket.on('error', function(err){
						if(err){
							throw err;
						}
					});
					
					socket.on('connection', function(socket){
						console.log("Se establecio conexión con " + host);
					});

					socket.on('data', function(data){
						console.log('Comando ' + host + ": " + data);

						socket.write(data);

						exec(data, puts);

						log = puts.toString();
							
						socket.write("\n");
						socket.write(log);
					});

					socket.on('close', function(data){
					
						console.log('Se Desconecto el host: ' + host);
					
					});
					
				}).listen(8080);

exports.serverTCP = serverTCP;